FROM ubuntu

RUN apt update -y && apt upgrade -y
RUN apt install -y apache2
RUN apt install -y ssh

WORKDIR /var/www/html

ADD index.html /var/www/html
ADD cm.sh /var/www/html
RUN chmod 777 /var/www/html/cm.sh

RUN ln -sf /bin/bash /bin/sh
RUN useradd -ms /bin/bash  alex
RUN echo -e "alex\nalex" | passwd
RUN usermod -aG sudo alex


EXPOSE 80
EXPOSE 22

VOLUME /var/www/html

ENTRYPOINT ["./cm.sh"]
#CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]


